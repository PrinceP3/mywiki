from django.db import models
from rest_framework import serializers
from django.contrib.auth.models import User
# Create your models here.


class Category(models.Model):
	name = models.CharField(max_length = 40, unique = True, null = False, blank = False)
	view = models.IntegerField(default = 0)
	created = models.DateTimeField(auto_now_add = True, auto_now = False)
	updated = models.DateTimeField(auto_now_add = False, auto_now = True)
	
	def __unicode__(self):
		return self.name

class Post(models.Model):
	status_list = (('1', 'Draft'), ('2', 'Public'), ('3', 'Private'))
	title = models.CharField(max_length = 150, null = False, blank = False)
	content = models.TextField(null = False, blank = False)
	view = models.IntegerField(default = 0)
	category = models.ForeignKey(Category)
	status = models.CharField(max_length = 8, choices = status_list)
	created = models.DateTimeField(auto_now_add = True, auto_now = False)
	updated = models.DateTimeField(auto_now_add = False, auto_now = True)

	def __unicode__(self):
		return self.title	

# Serializers define the API representation.
class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id' ,'name', 'view', 'created', 'updated')

class PostSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Post
		fields = ('id', 'title', 'view', 'category', 'status', 'content', 'created', 'updated')

class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('username', 'email', 'first_name', 'last_name', 'is_superuser', 'is_active', 'is_staff', 'date_joined', 'last_login')
		