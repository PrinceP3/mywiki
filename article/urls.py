from rest_framework import routers
from views import CategoryViewSet, PostViewSet, UserViewSet
# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'category', CategoryViewSet)
router.register(r'post', PostViewSet)
router.register(r'user', UserViewSet)