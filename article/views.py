from django.shortcuts import render
from django.contrib.auth.models import User
from models import CategorySerializer, Category, Post, PostSerializer, UserSerializer
from rest_framework import viewsets
# Create your views here.
# ViewSets define the view behavior.
class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserSerializer	