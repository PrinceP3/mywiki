from django.conf.urls import include, url
from django.contrib import admin
from article.urls import router


urlpatterns = [
    # Examples:
    # url(r'^$', 'mywiki.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^api/', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

]
