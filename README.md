# MyWiki - MediaWiki replacement

## Description
Simple blogging app, useful for documenting random stuffs.

## v0.1
### 1. Backend
- Add Post, Category CRUD features
- Restful API (via Django Rest Framework
- Authentication, Permission, Request Rate Limit.
- Image uploading

### 2. Frontend
- Design UI, responsive
- Single page app (either **AngularJS** or **EmberJS**

